// Update with your config settings.

require('dotenv').config();

module.exports = {
    client: 'pg',
    connection: {
	host: process.env.DB_HOST,
	port: process.env.DB_PORT,
	database: process.env.DB_DATABASE,
	user:     process.env.DB_USER,
	password: process.env.DB_PASSWORD
    },
    migrations: {
      directory: './migrations',
      tableName: 'knex_migrations',
    },
    seeds: {
      directory: './seeds',
    }
};
