const tableName = 'products_raw';
const columnName = 'name';

// Cannot run CONCURRENTLY feature inside a transaction block
exports.up = function(knex, Promise) {
   return knex.raw(`
DROP INDEX ${columnName}_idx;
`);
};

exports.down = function(knex, Promise) {
       return knex.raw(`
CREATE INDEX ${columnName}_idx ON ${tableName} (${columnName});
`);
};
