const schemaName = 'private';
const tableName = 'products_raw';

exports.up = function(knex, Promise) {
    return knex.raw(`
DROP TABLE IF EXISTS "${schemaName}"."${tableName}";

CREATE TABLE "${schemaName}"."${tableName}" (
    "id" SERIAL PRIMARY KEY NOT NULL,
    "createdAt" timestamptz DEFAULT CURRENT_TIMESTAMP NOT NULL,
    "updatedAt" timestamptz,
    "deletedAt" timestamptz,
    "name" character varying(255) NOT NULL,
    "description" text,
    "price" integer NOT NULL,
    "status" text
) WITH (oids = false);
`);
};

exports.down = function(knex, Promise) {
    return knex.raw(`
DROP TABLE IF EXISTS "${schemaName}"."${tableName}";
`);
};

