const tableName = 'products_raw';
const columnName = 'description';
const newColumnName = 'minor_description';

exports.up = function(knex, Promise) {
   return knex.raw(`
ALTER TABLE ${tableName} RENAME ${columnName} TO ${newColumnName};
`);
};

exports.down = function(knex, Promise) {
       return knex.raw(`
ALTER TABLE ${tableName} RENAME ${newColumnName} TO ${columnName};
`);
};
