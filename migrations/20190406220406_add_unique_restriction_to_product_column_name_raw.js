const tableName = 'products_raw';
const columnName = 'name';

exports.up = function(knex, Promise) {
   return knex.raw(`
ALTER TABLE ${tableName} ALTER COLUMN ${columnName} SET NOT NULL;
`);
};

exports.down = function(knex, Promise) {
       return knex.raw(`
ALTER TABLE ${tableName} ALTER COLUMN ${columnName} DROP NOT NULL;
`);
};
