const tableName = 'products';
const column = 'manufacturer_id';

exports.up = function(knex, Promise) {
    return knex.schema.hasColumn(tableName, column)
	.then(exists => {
            if (exists){
		return knex.schema.table(tableName, function (table) {
		    table.dropColumn(column);
		});
            }
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable(tableName, function(t) {
	t.integer(column).unique();
    });
};

