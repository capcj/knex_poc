const schemaName = 'private';

exports.up = function(knex, Promise) {
   return knex.raw(`
CREATE SCHEMA IF NOT EXISTS ${schemaName};
`);
};

exports.down = function(knex, Promise) {
       return knex.raw(`
DROP SCHEMA IF EXISTS ${schemaName};
`);
};
