const schemaName = 'private';
const tableName = 'products';

exports.up = function(knex, Promise) {
    return knex.schema.withSchema(schemaName).createTable(tableName, function(t) {
        t.increments('id').unsigned().primary();
        t.dateTime('createdAt').notNull().defaultTo(knex.fn.now());
        t.dateTime('updatedAt').nullable();
        t.dateTime('deletedAt').nullable();
        t.string('name').nullable();
        t.text('description').nullable();
	t.integer('price').notNull();
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.withSchema(schemaName).dropTableIfExists(tableName);
};
