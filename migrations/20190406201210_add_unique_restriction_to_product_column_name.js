const tableName = 'products';
const column = 'name';

exports.up = function(knex, Promise) {
    return  knex.schema.alterTable(tableName, function(t) {
	t.string(column).notNullable().alter();
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.hasColumn(tableName, column)
	.then(exists => {
            if (exists){
		return knex.schema.alterTable(tableName, function (t) {
		    t.string(column).nullable().alter();
		});
            }
        });
};

