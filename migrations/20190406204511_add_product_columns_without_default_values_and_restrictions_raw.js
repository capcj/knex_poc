const tableName = 'products_raw';
const typeName = 'status_availability_raw';
const column = 'status';

exports.up = function(knex, Promise) {
    return knex.raw(`
DROP TYPE IF EXISTS "${typeName}";

CREATE TYPE ${typeName} AS ENUM ('unavailable', 'available');

ALTER TABLE ${tableName} ADD COLUMN IF NOT EXISTS ${column} ${typeName}
`);
};

exports.down = function(knex, Promise) {
    return knex.raw(`
DROP TYPE IF EXISTS "${typeName}";

ALTER TABLE ${tableName} DROP COLUMN IF EXISTS ${column}
`);
};
