const tableName = 'products';
const column = 'serie_number';

exports.up = function(knex, Promise) {
  return knex.schema.alterTable(tableName, function(t) {
      t.integer(column).defaultTo(1);
  });
};

exports.down = function(knex, Promise) {
    return knex.schema.hasColumn(tableName, column)
	.then(exists => {
            if (exists){
		return knex.schema.table(tableName, function (table) {
		    table.dropColumn(column);
		});
            }
        });
};
