const tableName = 'categories';

exports.up = function(knex, Promise) {
    return knex.schema.dropTableIfExists(tableName);
};

exports.down = function(knex, Promise) {
    return knex.schema.createTable(tableName, function(t) {
        t.increments('id').unsigned().primary();
        t.dateTime('createdAt').notNull().defaultTo(knex.fn.now());
        t.dateTime('updatedAt').nullable();
        t.dateTime('deletedAt').nullable();
        t.string('name').notNull();
    });
};
