const tableName = 'products';
const column = 'description';
const newColumnName = 'minor_description';

exports.up = function(knex, Promise) {
    return knex.schema.hasColumn(tableName, column)
	.then(exists => {
            if (exists){
		return knex.schema.table(tableName, function (table) {
		    table.renameColumn(column, newColumnName);
		});
            }
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.hasColumn(tableName, newColumnName)
	.then(exists => {
            if (exists){
		return knex.schema.table(tableName, function (table) {
		    table.renameColumn(newColumnName, column);
		});
            }
        });
};


