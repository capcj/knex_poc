const tableName = 'categories_raw';

exports.up = function(knex, Promise) {
    return knex.raw(`
DROP TABLE IF EXISTS "${tableName}";
`);
};

exports.down = function(knex, Promise) {
    return knex.raw(`
DROP TABLE IF EXISTS "${tableName}";

CREATE TABLE "public"."${tableName}" (
    "id" SERIAL PRIMARY KEY NOT NULL,
    "createdAt" timestamptz DEFAULT CURRENT_TIMESTAMP NOT NULL,
    "updatedAt" timestamptz,
    "deletedAt" timestamptz,
    "name" character varying(255) NOT NULL
) WITH (oids = false);
`);
};
