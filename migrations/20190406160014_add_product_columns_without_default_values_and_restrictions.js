const tableName = 'products';
const typeName = 'status_availability';
const column = 'status';

exports.up = function(knex, Promise) {
  return knex.schema.alterTable(tableName, function(t) {
      t.enu(
	  column,
	  ['unavailable', 'available']
      );
  });
};

exports.down = function(knex, Promise) {
    return knex.schema.hasColumn(tableName, column)
	.then(exists => {
            if (exists){
		return knex.schema.table(tableName, function (table) {
		    table.dropColumn(column);
		});
            }
        });
};
