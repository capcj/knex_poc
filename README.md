# knex_poc

POC of KnexJs and PostgreSQL

[Knex.js](https://knexjs.org) is a multi-database SQL query builder, in this repo I will
work with some basic concepts from daily needs of a backend dev
to show the capabilities of the project and if can fulfill a basic
system requirement for this layer.

The [PostgreSQL](https://www.postgresql.org/) is used as database, a robust and almost with
full SQL compliance capabilities.

## To Run the migrations
```
knex migrate:latest
```

## To Rollback the migrations
```
knex migrate:rollback
```
